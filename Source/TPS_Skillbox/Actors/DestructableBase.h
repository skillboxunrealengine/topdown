// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "TPS_Skillbox/Interface/IGameActor.h"
#include "TPS_Skillbox/Effects/StateEffectBase.h"

#include "DestructableBase.generated.h"

UCLASS()
class TPS_SKILLBOX_API ADestructableBase : public AActor, public IIGameActor
{
	GENERATED_BODY()
	
public:	
	ADestructableBase();

protected:
	virtual void BeginPlay() override;

public:	
	virtual void Tick(float DeltaTime) override;

	EPhysicalSurface GetSurfaceType() override;	
	
	TArray<UStateEffectBase*> GetAllCurrentEffects() override;
	void RemoveEffect(UStateEffectBase* RemoveEffect)override;
	void AddEffect(UStateEffectBase* NewEffect)override;

	//Effect
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Setting")
	TArray<UStateEffectBase*> Effects;
};
