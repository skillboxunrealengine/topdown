
#include "DestructableBase.h"
#include "Materials/MaterialInterface.h"
#include "PhysicalMaterials/PhysicalMaterial.h"

ADestructableBase::ADestructableBase()
{
	PrimaryActorTick.bCanEverTick = true;

}

void ADestructableBase::BeginPlay()
{
	Super::BeginPlay();
	
}

void ADestructableBase::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

EPhysicalSurface ADestructableBase::GetSurfaceType()
{
	EPhysicalSurface result = EPhysicalSurface::SurfaceType1;

	UStaticMeshComponent* Mesh = Cast<UStaticMeshComponent>(GetComponentByClass(UStaticMeshComponent::StaticClass()));
	if (Mesh) {
		UMaterialInterface* Material = Mesh->GetMaterial(0);
		if (Material) {
			result = Material->GetPhysicalMaterial()->SurfaceType;
		}
	}
	return result;
}

TArray<UStateEffectBase*> ADestructableBase::GetAllCurrentEffects()
{
	return Effects;
}

void ADestructableBase::RemoveEffect(UStateEffectBase* RemoveEffect)
{
	Effects.Remove(RemoveEffect);
}

void ADestructableBase::AddEffect(UStateEffectBase* NewEffect)
{
	Effects.Add(NewEffect);
}