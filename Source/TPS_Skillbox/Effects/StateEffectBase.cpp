#include "StateEffectBase.h"
#include "TPS_Skillbox/Components/HealthSystemComponent.h"
#include "TPS_Skillbox/Interface/IGameActor.h"
#include "Components/SkeletalMeshComponent.h"
#include "Kismet/GameplayStatics.h"

bool UStateEffectBase::InitObject(AActor* Actor)
{
	
	myActor = Actor;

	IIGameActor* myInterface = Cast<IIGameActor>(myActor);
	if (myInterface)
	{
		myInterface->AddEffect(this);
	}

	return true;
}

void UStateEffectBase::DestroyObject()
{
	IIGameActor* myInterface = Cast<IIGameActor>(myActor);
	if (myInterface)
	{
		myInterface->RemoveEffect(this);
	}

	myActor = nullptr;
	if (this && this->IsValidLowLevel())
	{
		this->ConditionalBeginDestroy();
	}	
}

FVector UStateEffectBase::IndicatorLocation()
{
	FVector result = FVector((myActor->GetActorLocation().X + FMath::FRandRange(-60.0f, 60.0f)), (myActor->GetActorLocation().Y + FMath::FRandRange(-60.0f, 60.0f)), (myActor->GetActorLocation().Z + FMath::FRandRange(-60.0f, 60.0f)));
	return result;
}

/// ExecuteOnce
//bool UStateEffect_ExecuteOnce::InitObject(AActor* Actor)
//{
//	Super::InitObject(Actor);
//	if (Cast<UHealthSystemComponent>(myActor->GetComponentByClass(UHealthSystemComponent::StaticClass()))) {
//		ExecuteOnce();
//	}
//	else {
//		DestroyObject();
//		return false;
//	}
//	return true;
//}
//
//void UStateEffect_ExecuteOnce::DestroyObject()
//{
//	Super::DestroyObject();
//}
//
//void UStateEffect_ExecuteOnce::ExecuteOnce()
//{
//	if (myActor)
//	{
//		UHealthSystemComponent* HealthComponent = Cast<UHealthSystemComponent>(myActor->GetComponentByClass(UHealthSystemComponent::StaticClass()));
//		if (HealthComponent)
//		{
//			HealthComponent->ChangeCurrentHealth(Power, false, IndicatorLocation());
//		}
//	}	
//
//	DestroyObject();
//}

///ExecuteTimer
//bool UStateEffect_ExecuteTimer::InitObject(AActor* Actor)
//{
//	Super::InitObject(Actor);
//
//	if (Cast<UHealthSystemComponent>(myActor->GetComponentByClass(UHealthSystemComponent::StaticClass()))) {
//
//		GetWorld()->GetTimerManager().SetTimer(TimerHandle_EffectTimer, this, &UStateEffect_ExecuteTimer::DestroyObject, Timer, false);
//		GetWorld()->GetTimerManager().SetTimer(TimerHandle_ExecuteTimer, this, &UStateEffect_ExecuteTimer::Execute, RateTime, true);
//
//		if (ParticleEffect)
//		{
//			FName NameBoneToAttached;
//			FVector Loc = FVector(0);
//
//			ParticleEmitter = UGameplayStatics::SpawnEmitterAttached(ParticleEffect, myActor->GetRootComponent(), NameBoneToAttached, Loc, FRotator::ZeroRotator, EAttachLocation::SnapToTarget, false);
//		}
//	}
//	else {
//		DestroyObject();
//		return false;
//	}
//	
//	return true;
//}
//
//void UStateEffect_ExecuteTimer::DestroyObject()
//{
//	if (ParticleEmitter) {
//		ParticleEmitter->DestroyComponent();
//		ParticleEmitter = nullptr;
//	}
//	Super::DestroyObject();
//}
//
//void UStateEffect_ExecuteTimer::Execute()
//{
//	if (myActor)
//	{
//		//UGameplayStatics::ApplyDamage(myActor,Power,nullptr,nullptr,nullptr);	
//		UHealthSystemComponent* HealthComponent = Cast<UHealthSystemComponent>(myActor->GetComponentByClass(UHealthSystemComponent::StaticClass()));
//		if (HealthComponent)
//		{
//			HealthComponent->ChangeCurrentHealth(Power, false, IndicatorLocation());
//		}
//	}
//}

/// ExecuteByTime
bool UStateEffect_Invincible::InitObject(AActor* Actor)
{
	Super::InitObject(Actor);

	if (Cast<UHealthSystemComponent>(myActor->GetComponentByClass(UHealthSystemComponent::StaticClass()))) {
		Execute();
		GetWorld()->GetTimerManager().SetTimer(TimerHandle_EffectTimer, this, &UStateEffect_Invincible::DestroyObject, Time, false);

		if (ParticleEffect)
		{
			FName NameBoneToAttached;
			FVector Loc = FVector(0);

			USkeletalMeshComponent* myMesh = Cast<USkeletalMeshComponent>(myActor->GetComponentByClass(USkeletalMeshComponent::StaticClass()));
			if (myMesh) {
				ParticleEmitter = UGameplayStatics::SpawnEmitterAttached(ParticleEffect, myMesh, NameBoneToAttached, Loc, FRotator::ZeroRotator, EAttachLocation::SnapToTarget, false);
			}
			else {
				ParticleEmitter = UGameplayStatics::SpawnEmitterAttached(ParticleEffect, myActor->GetRootComponent(), NameBoneToAttached, Loc, FRotator::ZeroRotator, EAttachLocation::SnapToTarget, false);
			}
		}
	}
	else {
		DestroyObject();
		return false;
	}

	return true;
}

void UStateEffect_Invincible::DestroyObject()
{
	if (ParticleEmitter) {
		ParticleEmitter->DestroyComponent();
		ParticleEmitter = nullptr;
	}
	UHealthSystemComponent* HealthComponent = Cast<UHealthSystemComponent>(myActor->GetComponentByClass(UHealthSystemComponent::StaticClass()));
	if (HealthComponent) {
		HealthComponent->DamageCut = HealthComponent->InitDamageCut;
	}
	Super::DestroyObject();
}

void UStateEffect_Invincible::Execute()
{
	if (myActor)
	{
		UHealthSystemComponent* HealthComponent = Cast<UHealthSystemComponent>(myActor->GetComponentByClass(UHealthSystemComponent::StaticClass()));
		if (HealthComponent)
		{
			HealthComponent->DamageCut = 0.0f;
		}
	}
}


/// AuraDamage
bool UStateEffect_AuraDamage::InitObject(AActor* Actor)
{
	Super::InitObject(Actor);

	GetWorld()->GetTimerManager().SetTimer(TimerHandle_EffectTimer, this, &UStateEffect_AuraDamage::DestroyObject, Timer, false);
	GetWorld()->GetTimerManager().SetTimer(TimerHandle_ExecuteTimer, this, &UStateEffect_AuraDamage::Execute, RateTime, true);

	if (ParticleEffect)
	{
		FName NameBoneToAttached;
		FVector Loc = FVector(0);

		USkeletalMeshComponent* myMesh = Cast<USkeletalMeshComponent>(myActor->GetComponentByClass(USkeletalMeshComponent::StaticClass()));
		if (myMesh) {
			ParticleEmitter = UGameplayStatics::SpawnEmitterAttached(ParticleEffect, myMesh, NameBoneToAttached, Loc, FRotator::ZeroRotator, EAttachLocation::SnapToTarget, false);
		}
		else {
			ParticleEmitter = UGameplayStatics::SpawnEmitterAttached(ParticleEffect, myActor->GetRootComponent(), NameBoneToAttached, Loc, FRotator::ZeroRotator, EAttachLocation::SnapToTarget, false);
		}
	}
	else {
		DestroyObject();
		return false;
	}

	return true;
}

void UStateEffect_AuraDamage::DestroyObject()
{
	if (ParticleEmitter) {
		ParticleEmitter->DestroyComponent();
		ParticleEmitter = nullptr;
	}
	Super::DestroyObject();
}

void UStateEffect_AuraDamage::Execute()
{
	if (myActor)
	{
		UE_LOG(LogTemp, Warning, TEXT("Executed"));
		TArray<AActor*> IgnoredActors;
		IgnoredActors.Add(myActor);
		UGameplayStatics::ApplyRadialDamage(GetWorld(), Power, myActor->GetActorLocation(), Radius, NULL, IgnoredActors, myActor, nullptr, true);
	}
}