// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "HealthSystemComponent.h"
#include "CharacterHealthSystemComponent.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE_ThreeParams(FOnShieldChange, float, MaxShieldCapacity, float, Damage, float, ShieldCapacity);

UCLASS()
class TPS_SKILLBOX_API UCharacterHealthSystemComponent : public UHealthSystemComponent
{
	GENERATED_BODY()
	
public:

	FTimerHandle TimerHandle_ShieldRepairCooldown;

	UPROPERTY(BlueprintAssignable, EditAnywhere, BlueprintReadWrite, Category = "Health System /// CPP VARIABLES")
	FOnShieldChange OnShieldChange;

	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;
	UFUNCTION(BlueprintCallable)
	void ShieldRegenerateTick(float DeltaTime);

	void ChangeCurrentHealth(float HealthValue, bool IsPiercing = false, FVector HitLocation = FVector(0.0f)) override;

	UFUNCTION(BlueprintCallable)
	float ChangeShieldCapacity(float HealthValue, bool IsPiercing = false);

	UFUNCTION(BlueprintCallable)
	float GetCurrentShieldCapacity();
	UFUNCTION(BlueprintCallable)
	float GetMaxShieldCapacity();


	UFUNCTION(BlueprintCallable)
	void EnableShieldRegenerate();
	UFUNCTION(BlueprintCallable)
	void SetShieldRegenerate(bool Enable);

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Health System /// CPP VARIABLES")
	float MaxShieldCapacity = 100.0f;

	float ShieldRegenerationCooldown = 5.0f;

	bool IsShieldRegenerate = false;
	float TimeToRegenerateShield = 1.0f;
	float TimerToRegenerateShield = 0.0f;

protected:

	float ShieldCapacity = MaxShieldCapacity;

	float ShieldRepair = 5.0f;
};
