// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "HealthSystemComponent.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE_FourParams(FOnHealthChange, float, MaxHealth, float, Health, float, Damage, FVector, HitLocation);
DECLARE_DYNAMIC_MULTICAST_DELEGATE(FOnDead);

UCLASS(ClassGroup = (Custom), meta = (BlueprintSpawnableComponent))
class TPS_SKILLBOX_API UHealthSystemComponent : public UActorComponent
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	UHealthSystemComponent();

	UPROPERTY(BlueprintAssignable, EditAnywhere, BlueprintReadWrite, Category = "Health System /// CPP VARIABLES")
	FOnHealthChange OnHealthChange;
	UPROPERTY(BlueprintAssignable, EditAnywhere, BlueprintReadWrite, Category = "Health System /// CPP VARIABLES")
	FOnDead OnDead;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Health System /// CPP VARIABLES")
	float MaxHealth = 100.0f;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Health System /// CPP VARIABLES")
	float InitDamageCut = 1.0f;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Health System /// CPP VARIABLES")
	float DamageCut;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Health System /// CPP VARIABLES")
	bool IsAlive = true;

protected:
	// Called when the game starts
	virtual void BeginPlay() override;

	float Health;
public:	
	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

	UFUNCTION(BlueprintCallable, BlueprintPure, Category = "Health System /// CPP VARIABLES")
	float GetMaxHealth();

	UFUNCTION(BlueprintCallable, BlueprintPure, Category = "Health System /// CPP VARIABLES")
	float GetCurrentHealth();
	UFUNCTION(BlueprintCallable, Category = "Health System /// CPP VARIABLES")
	void SetCurrentHealth(float HealthValue);
	UFUNCTION(BlueprintCallable, Category = "Health System /// CPP VARIABLES")
	virtual void ChangeCurrentHealth(float HealthValue, bool IsPiercing = false, FVector HitLocation = FVector(0.0f));
	UFUNCTION(BlueprintNativeEvent)
	void DeadEvent();
};
