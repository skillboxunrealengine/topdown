
#include "HealthSystemComponent.h"

UHealthSystemComponent::UHealthSystemComponent()
{

	PrimaryComponentTick.bCanEverTick = true;

}


void UHealthSystemComponent::BeginPlay()
{
	Super::BeginPlay();

	DamageCut = InitDamageCut;
	Health = MaxHealth;
	
}


void UHealthSystemComponent::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

}

float UHealthSystemComponent::GetMaxHealth()
{
	return MaxHealth;
}

float UHealthSystemComponent::GetCurrentHealth()
{
	return Health;
}

void UHealthSystemComponent::SetCurrentHealth(float HealthValue)
{
	Health = HealthValue;
}

void UHealthSystemComponent::ChangeCurrentHealth(float HealthValue, bool IsPiercing, FVector HitLocation)
{
	if (IsAlive) {
		float CorrectHealth;
		if (IsPiercing) {
			CorrectHealth = HealthValue;
		}
		else {
			if (HealthValue > 0) {
				CorrectHealth = HealthValue;
			}
			else {
				CorrectHealth = HealthValue * DamageCut;
			}
		}
		Health = Health + CorrectHealth;

		OnHealthChange.Broadcast(MaxHealth, Health, CorrectHealth, HitLocation);

		if (Health > MaxHealth) {
			Health = MaxHealth;
		}
		else {
			if (Health <= 0) {
				OnDead.Broadcast();
				DeadEvent();
				IsAlive = false;
			}
		}
	}
}

void UHealthSystemComponent::DeadEvent_Implementation()
{
}

