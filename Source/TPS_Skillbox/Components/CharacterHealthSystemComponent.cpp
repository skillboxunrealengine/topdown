// Fill out your copyright notice in the Description page of Project Settings.


#include "CharacterHealthSystemComponent.h"

void UCharacterHealthSystemComponent::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	ShieldRegenerateTick(DeltaTime);
}

void UCharacterHealthSystemComponent::ChangeCurrentHealth(float HealthValue, bool IsPiercing, FVector HitLocation)
{
	if (IsAlive) {
		float CorrectValue;
		if (IsPiercing) {
			CorrectValue = HealthValue;
			if (HealthValue <= 0) {
				CorrectValue = ChangeShieldCapacity(CorrectValue, true);
				SetShieldRegenerate(false);
				GetWorld()->GetTimerManager().SetTimer(TimerHandle_ShieldRepairCooldown, this, &UCharacterHealthSystemComponent::EnableShieldRegenerate, ShieldRegenerationCooldown, false);
				TimerToRegenerateShield = 0.0f;
			}
		}
		else {
			if (HealthValue > 0) {
				CorrectValue = HealthValue;
			}
			else {
				CorrectValue = HealthValue * DamageCut;
				CorrectValue = ChangeShieldCapacity(CorrectValue, true);
				SetShieldRegenerate(false);
				GetWorld()->GetTimerManager().SetTimer(TimerHandle_ShieldRepairCooldown, this, &UCharacterHealthSystemComponent::EnableShieldRegenerate, ShieldRegenerationCooldown, false);
				TimerToRegenerateShield = 0.0f;
			}
		}
		Health = Health + CorrectValue;

		OnHealthChange.Broadcast(MaxHealth, Health, CorrectValue, HitLocation);

		if (Health > MaxHealth) {
			Health = MaxHealth;
		}
		else {
			if (Health <= 0) {
				OnDead.Broadcast();
				DeadEvent();
				IsAlive = false;
			}
		}
	}
}

float UCharacterHealthSystemComponent::GetCurrentShieldCapacity()
{
	return ShieldCapacity;
}

float UCharacterHealthSystemComponent::GetMaxShieldCapacity()
{
	return MaxShieldCapacity;
}

float UCharacterHealthSystemComponent::ChangeShieldCapacity(float HealthValue, bool IsPiercing)
{	
	float ShieldBroken = 0;
	if (IsAlive) {
		float CorrectValue = 0;
		float RealShieldCapacity = 0;
		if (IsPiercing) {
			ShieldCapacity = ShieldCapacity + HealthValue;
			CorrectValue = HealthValue;

			if (ShieldCapacity > MaxShieldCapacity) {
				ShieldCapacity = MaxShieldCapacity;
			}
			else if (ShieldCapacity < 0) {
				ShieldBroken = ShieldCapacity;
				ShieldCapacity = 0.0f;
			}
		}
		else {
			RealShieldCapacity = (ShieldCapacity * (1 + DamageCut)) + HealthValue;
			ShieldCapacity = ShieldCapacity + (HealthValue * DamageCut);

			CorrectValue = HealthValue * DamageCut;

			if (ShieldCapacity > MaxShieldCapacity) {
				ShieldCapacity = MaxShieldCapacity;
			}
			else if (ShieldCapacity < 0) {
				ShieldBroken = RealShieldCapacity;
				ShieldCapacity = 0.0f;
			}
		}

		OnShieldChange.Broadcast(MaxShieldCapacity, CorrectValue, ShieldCapacity);
	}
	return ShieldBroken;
}

void UCharacterHealthSystemComponent::ShieldRegenerateTick(float DeltaTime)
{
	if (IsShieldRegenerate && TimerToRegenerateShield >= TimeToRegenerateShield && ShieldCapacity < MaxShieldCapacity) {
		TimerToRegenerateShield = 0.0f;
		ChangeShieldCapacity(ShieldRepair);
		UE_LOG(LogTemp, Warning, TEXT("Shield + 5"));
	}
	else {
		TimerToRegenerateShield += DeltaTime;
	}

}

void UCharacterHealthSystemComponent::EnableShieldRegenerate()
{
	UE_LOG(LogTemp, Warning, TEXT("ShieldRegenerationENABLED"));
	IsShieldRegenerate = true;
	GetWorld()->GetTimerManager().ClearTimer(TimerHandle_ShieldRepairCooldown);
}

void UCharacterHealthSystemComponent::SetShieldRegenerate(bool Enable)
{
	IsShieldRegenerate = Enable;
}
