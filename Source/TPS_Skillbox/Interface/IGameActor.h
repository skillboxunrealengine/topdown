#pragma once

#include "CoreMinimal.h"
#include "UObject/Interface.h"
#include "TPS_Skillbox/Effects/StateEffectBase.h"

#include "IGameActor.generated.h"

UINTERFACE(MinimalAPI)
class UIGameActor : public UInterface
{
	GENERATED_BODY()
};

/**
 * 
 */
class TPS_SKILLBOX_API IIGameActor
{
	GENERATED_BODY()

	// Add interface functions to this class. This is the class that will be inherited to implement this interface.
public:

	virtual EPhysicalSurface GetSurfaceType();

	virtual TArray<UStateEffectBase*> GetAllCurrentEffects();
	virtual void RemoveEffect(UStateEffectBase* RemoveEffect);
	virtual void AddEffect(UStateEffectBase* NewEffect);
};
