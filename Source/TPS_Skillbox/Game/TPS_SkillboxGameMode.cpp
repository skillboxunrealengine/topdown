// Copyright Epic Games, Inc. All Rights Reserved.

#include "TPS_SkillboxGameMode.h"
#include "TPS_Skillbox/Character/TPS_SkillboxPlayerController.h"
#include "TPS_Skillbox/Character/TPS_SkillboxCharacter.h"
#include "UObject/ConstructorHelpers.h"

ATPS_SkillboxGameMode::ATPS_SkillboxGameMode()
{
	// use our custom PlayerController class
	PlayerControllerClass = ATPS_SkillboxPlayerController::StaticClass();

	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnBPClass(TEXT("/Content/TopDown/Character/Blueprints/BP_TopDownCharacter"));
	if (PlayerPawnBPClass.Class != nullptr)
	{
		DefaultPawnClass = PlayerPawnBPClass.Class;
	}

	// set default controller to our Blueprinted controller
	static ConstructorHelpers::FClassFinder<APlayerController> PlayerControllerBPClass(TEXT("/Content/TopDown/Character/Blueprints/BP_TopDownPlayerController"));
	if(PlayerControllerBPClass.Class != NULL)
	{
		PlayerControllerClass = PlayerControllerBPClass.Class;
	}
}