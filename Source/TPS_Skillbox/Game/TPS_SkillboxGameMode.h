// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "TPS_SkillboxGameMode.generated.h"

UCLASS(minimalapi)
class ATPS_SkillboxGameMode : public AGameModeBase
{
	GENERATED_BODY()

public:
	ATPS_SkillboxGameMode();
};



