#pragma once

#include "Kismet/BlueprintFunctionLibrary.h"
#include "GameFramework/PlayerState.h"
#include "Engine/DataTable.h"
#include "PlayerStates.generated.h"

UENUM(BlueprintType)
enum class EMovementState : uint8
{
	Walk_state UMETA(DisplayName = "Walk state"),
	Run_state UMETA(DisplayName = "Run state"),
	Aim_state UMETA(DisplayName = "Aim state"),
};

USTRUCT(BlueprintType)
struct FCharacterSpeed
{
	GENERATED_BODY()

	UPROPERTY()
	float WalkSpeed = 400.0f;
	UPROPERTY()
	float RunSpeed = 500.0f;
	UPROPERTY()
	float AimSpeed = 100.0f;
};

UENUM(BlueprintType)
enum class EWeaponType : uint8
{
	EmptyType UMETA(DisplayName = "NoneType"),
	RiffleType UMETA(DisplayName = "Riffle"),
	ShotgunType UMETA(DisplayName = "Shotgun"),
	PistolType UMETA(DisplayName = "Pistol"),
	GrenadeLauncherType UMETA(DisplayName = "GrenadeLauncher"),
};

USTRUCT(BlueprintType)
struct FProjectileInfo
{
	GENERATED_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "CPP VARIABLES / PROJECTILE SETTINGS")
	UStaticMesh* BulletMesh = nullptr;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "CPP VARIABLES / PROJECTILE SETTINGS")
	UParticleSystem* BulletFX = nullptr;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "CPP VARIABLES / PROJECTILE SETTINGS")
	TSubclassOf<class AProjectileDefaults> Projectile = nullptr;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "CPP VARIABLES / PROJECTILE SETTINGS")
	float ProjectileDamage = 20.0f;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "CPP VARIABLES / PROJECTILE SETTINGS")
	float ProjectileLifeTime = 5.0f;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "CPP VARIABLES / PROJECTILE SETTINGS")
	float ProjectileInitSpeed = 2000.0f;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "CPP VARIABLES / PROJECTILE SETTINGS")
	float ProjectileMaxRadiusDamage = 20.0f;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "CPP VARIABLES / PROJECTILE SETTINGS")
	UParticleSystem* ExplodeFX = nullptr;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "CPP VARIABLES / PROJECTILE SETTINGS")
	USoundBase* ExplodeSound = nullptr;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "CPP VARIABLES / PROJECTILE SETTINGS")
	float ExplodeMinDist = 300.0f;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "CPP VARIABLES / PROJECTILE SETTINGS")
	float ExplodeMaxDist = 1000.0f;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "CPP VARIABLES / PROJECTILE SETTINGS")
	float ExplodeMaxDamage = 50.0f;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "CPP VARIABLES / PROJECTILE SETTINGS")
	float ExplodeMinDamageMultiplier = 0.1f;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "CPP VARIABLES / PROJECTILE SETTINGS")
	TMap<TEnumAsByte<EPhysicalSurface>, UMaterialInterface*> HitDecals;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "CPP VARIABLES / PROJECTILE SETTINGS")
	TMap<TEnumAsByte<EPhysicalSurface>, UParticleSystem*> HitFX;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Effect")
	TSubclassOf<class UStateEffectBase> Effect = nullptr;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "CPP VARIABLES / PROJECTILE SETTINGS")
	USoundBase* HitSound = nullptr;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "CPP VARIABLES / PROJECTILE SETTINGS")
	bool IsDebug = false;
};

USTRUCT(BlueprintType)
struct FWeaponDispersion
{
	GENERATED_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "CPP VARIABLES / DISPERSION SETTINGS")
	float Aim_DispersionMax = 1.0f;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "CPP VARIABLES / DISPERSION SETTINGS")
	float Aim_DispersionMin = 0.1f;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "CPP VARIABLES / DISPERSION SETTINGS")
	float Aim_DispersionRecoil = 1.0f;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "CPP VARIABLES / DISPERSION SETTINGS")
	float Aim_DispersionReduction = 1.0f;


	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "CPP VARIABLES / DISPERSION SETTINGS")
	float Walk_DispersionMax = 1.0f;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "CPP VARIABLES / DISPERSION SETTINGS")
	float Walk_DispersionMin = 0.1f;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "CPP VARIABLES / DISPERSION SETTINGS")
	float Walk_DispersionRecoil = 1.0f;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "CPP VARIABLES / DISPERSION SETTINGS")
	float Walk_DispersionReduction = 1.0f;
};

USTRUCT(BlueprintType)
struct FDropMeshInfo
{
	GENERATED_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "CPP VARIABLES / WEAPON SETTINGS")
	UStaticMesh* DropMesh = nullptr;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "CPP VARIABLES / WEAPON SETTINGS")
	float DropMeshTime = -1.0f;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "CPP VARIABLES / WEAPON SETTINGS")
	float DropMeshLifeTime = 5.0f;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "CPP VARIABLES / WEAPON SETTINGS")
	float DropMeshImpulse = 0.0f;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "CPP VARIABLES / WEAPON SETTINGS")
	float DropMeshImpulseDispersion = 0.0f;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "CPP VARIABLES / WEAPON SETTINGS")
	float DropMeshMass = 0.0f;
};

USTRUCT(BlueprintType)
struct FWeaponInfo : public	FTableRowBase
{
	GENERATED_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "CPP VARIABLES / WEAPON SETTINGS")
	USkeletalMesh* WeaponSkeletalMesh = nullptr;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "CPP VARIABLES / WEAPON SETTINGS")
	UStaticMesh* WeaponStaticMesh = nullptr;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "CPP VARIABLES / WEAPON SETTINGS")
	TSubclassOf<class AWeaponDefaults> WeaponClass = nullptr;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "CPP VARIABLES / WEAPON SETTINGS")
	float RateOfFire = 0.25f;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "CPP VARIABLES / WEAPON SETTINGS")
	float ReloadTime = 2.0f;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "CPP VARIABLES / WEAPON SETTINGS")
	int32 MaxRounds = 30;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "CPP VARIABLES / WEAPON SETTINGS")
	int32 RoundPerShot = 1;


	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "CPP VARIABLES / WEAPON SETTINGS")
	FWeaponDispersion DispersionWeapon;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "CPP VARIABLES / WEAPON SETTINGS")
	USoundBase* FireSound = nullptr;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "CPP VARIABLES / WEAPON SETTINGS")
	USoundBase* ReloadWeapon = nullptr;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "CPP VARIABLES / WEAPON SETTINGS")
	USoundBase* ReloadWeaponEnd = nullptr;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "CPP VARIABLES / WEAPON SETTINGS")
	UParticleSystem* FireParticles = nullptr;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "CPP VARIABLES / PROJECTILE SETTINGS")
	FProjectileInfo ProjectileSetting;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "CPP VARIABLES / WEAPON SETTINGS")
	float WeaponDamage = 20.0f;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "CPP VARIABLES / PROJECTILE SETTINGS")
	float DistanceTrace = 2500.0f;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "CPP VARIABLES / WEAPON SETTINGS")
	UDecalComponent* DecalOnHit = nullptr;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "CPP VARIABLES / WEAPON SETTINGS")
	UAnimMontage* PlayerFireAnimation = nullptr;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "CPP VARIABLES / WEAPON SETTINGS")
	UAnimMontage* PlayerFireAimAnimation = nullptr;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "CPP VARIABLES / WEAPON SETTINGS")
	UAnimMontage* PlayerReloadAnimation = nullptr;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "CPP VARIABLES / WEAPON SETTINGS")
	UAnimMontage* WeaponReloadAnimation = nullptr;


	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "CPP VARIABLES / WEAPON SETTINGS")
	FDropMeshInfo ClipDropMesh;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "CPP VARIABLES / WEAPON SETTINGS")
	FDropMeshInfo ShellBulletMesh;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "CPP VARIABLES / WEAPON SETTINGS")
	float SwitchTimeToWeapon = 1.0f;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "CPP VARIABLES / WEAPON SETTINGS")
	UTexture2D* WeaponIcon = nullptr;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "CPP VARIABLES / WEAPON SETTINGS")
	EWeaponType WeaponType;

};

USTRUCT(BlueprintType)
struct FAdditionalWeaponInfo
{
	GENERATED_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "CPP VARIABLES / WEAPON SETTINGS")
	int32 Round = 0;
};

USTRUCT(BlueprintType)
struct FWeaponSlot
{
	GENERATED_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "CPP VARIABLES / WEAPON SETTINGS")
	FName NameItem;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "CPP VARIABLES / WEAPON SETTINGS")
	EWeaponType WeaponType = EWeaponType::RiffleType;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "CPP VARIABLES / WEAPON SETTINGS")
	FAdditionalWeaponInfo AdditionalInfo;
};

USTRUCT(BlueprintType)
struct FAmmoSlot
{
	GENERATED_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "CPP VARIABLES / WEAPON SETTINGS")
	EWeaponType WeaponType = EWeaponType::RiffleType;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "CPP VARIABLES / WEAPON SETTINGS")
	int32 Count = 100;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "CPP VARIABLES / WEAPON SETTINGS")
	int32 MaxCount = 100;
};

USTRUCT(BlueprintType)
struct FDropItem : public FTableRowBase {
	GENERATED_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "CPP VARIABLES / DROP WEAPON SETTINGS")
	UStaticMesh* WeaponStaticMesh = nullptr;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "CPP VARIABLES / DROP WEAPON SETTINGS")
	USkeletalMesh* WeaponSkeletalMesh = nullptr;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "CPP VARIABLES / DROP WEAPON SETTINGS")
	FWeaponSlot WeaponInfo;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "CPP VARIABLES / DROP WEAPON SETTINGS")
	UParticleSystem* InteractableParticle = nullptr;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "CPP VARIABLES / DROP WEAPON SETTINGS")
	UMaterialInterface* MaterialOverlay = nullptr;

};


UCLASS(Blueprintable)
class TPS_SKILLBOX_API APlayerStates : public APlayerState
{
	GENERATED_BODY()

public:
	UFUNCTION(BlueprintCallable)
	static void AddEffectBySurfaceType(AActor* TargetEffectActor, TSubclassOf<UStateEffectBase> AddEffectClass, EPhysicalSurface SurfaceType);
};
