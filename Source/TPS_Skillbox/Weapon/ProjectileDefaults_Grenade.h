// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "ProjectileDefaults.h"
#include "ProjectileDefaults_Grenade.generated.h"

/**
 * 
 */
UCLASS()
class TPS_SKILLBOX_API AProjectileDefaults_Grenade : public AProjectileDefaults
{
	GENERATED_BODY()
	
protected:
	virtual void BeginPlay() override;

public:
	virtual void Tick(float DeltaTime) override;

	void TimerExplode(float DeltaTime);

	virtual void BulletCollisionSphereHit(class UPrimitiveComponent* HitComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, FVector NormalImpulse, const FHitResult& Hit);

	virtual void ImpactProjectile() override;

	void Explode();

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "CPP VAR")
	bool ExplodeOnImpact = false;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "CPP VAR")
	float GravityScale = 2.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "CPP VAR")
	bool TimerEnabled = false;
	float TimerToExplode = 0.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "CPP VAR")
	float TimeToExplode = 5.0f;
};
