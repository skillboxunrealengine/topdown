// Fill out your copyright notice in the Description page of Project Settings.


#include "ProjectileDefaults_Grenade.h"
#include "Kismet/GameplayStatics.h"

void AProjectileDefaults_Grenade::BeginPlay()
{
	Super::BeginPlay();
	BulletMesh->SetCollisionProfileName(TEXT("NoCollision"));
	BulletProjectileMovement->ProjectileGravityScale = GravityScale;
}

void AProjectileDefaults_Grenade::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	TimerExplode(DeltaTime);
}

void AProjectileDefaults_Grenade::TimerExplode(float DeltaTime)
{
	if (TimerEnabled) {
		if (TimerToExplode > TimeToExplode) {
			Explode();
		}
		else {
			TimerToExplode += DeltaTime;
		}
	}
}

void AProjectileDefaults_Grenade::BulletCollisionSphereHit(UPrimitiveComponent* HitComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, FVector NormalImpulse, const FHitResult& Hit)
{
	Super::BulletCollisionSphereHit(HitComp, OtherActor, OtherComp, NormalImpulse, Hit);
}

void AProjectileDefaults_Grenade::ImpactProjectile()
{
	if (ExplodeOnImpact) {
		Explode();
	}
	else {
		TimerEnabled = true;
	}
}

void AProjectileDefaults_Grenade::Explode()
{
	UE_LOG(LogTemp, Warning, TEXT("babax"));
	TimerEnabled = false;
	if (ProjectileSetting.ExplodeFX) {
		UGameplayStatics::SpawnEmitterAtLocation(GetWorld(), ProjectileSetting.ExplodeFX, GetActorLocation(), GetActorRotation());
	}
	if (ProjectileSetting.ExplodeSound) {
		UGameplayStatics::PlaySoundAtLocation(GetWorld(), ProjectileSetting.ExplodeSound, GetActorLocation());
	}

	TArray<AActor*> IgnoredActor;
	FVector ExplodeLocation = GetActorLocation() + FVector(0.0f, 0.0f, 10.0f);
	UGameplayStatics::ApplyRadialDamageWithFalloff(GetWorld(), ProjectileSetting.ExplodeMaxDamage, ProjectileSetting.ExplodeMaxDamage * ProjectileSetting.ExplodeMinDamageMultiplier, ExplodeLocation, ProjectileSetting.ExplodeMinDist, ProjectileSetting.ExplodeMaxDist, 5.0f, NULL, IgnoredActor, this, GetInstigatorController());
	if (ProjectileSetting.IsDebug) {
		DrawDebugSphere(GetWorld(), ExplodeLocation, ProjectileSetting.ExplodeMinDist, 20, FColor::Emerald, false, 2.0f, 4, 0.0f);
		DrawDebugSphere(GetWorld(), ExplodeLocation, ProjectileSetting.ExplodeMaxDist, 20, FColor::Cyan, false, 2.0f, 4, 0.0f);
	}

	Destroy();
}
