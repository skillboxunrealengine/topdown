// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Components/ArrowComponent.h"
#include "TPS_Skillbox/PlayerStates.h"
#include "ProjectileDefaults.h"
#include "WeaponDefaults.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FOnWeaponFireStart, UAnimMontage*, AnimFire, UAnimMontage*, AnimAimFire);
DECLARE_DYNAMIC_MULTICAST_DELEGATE(FOnWeaponFireEnd);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnWeaponReloadStart, UAnimMontage*, Anim);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FOnWeaponReloadEnd, bool, IsSuccess, int32, KeepAmmo);

UCLASS()
class TPS_SKILLBOX_API AWeaponDefaults : public AActor
{
	GENERATED_BODY()
	
public:
	// Sets default values for this actor's properties
	AWeaponDefaults();

	FOnWeaponFireStart OnWeaponFireStart;
	FOnWeaponFireEnd OnWeaponFireEnd;
	FOnWeaponReloadStart OnWeaponReloadStart;
	FOnWeaponReloadEnd OnWeaponReloadEnd;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"), Category = "CPP VARIABLES / WEAPON SETTINGS")
	class USceneComponent* SceneComponent = nullptr;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"), Category = "CPP VARIABLES / WEAPON SETTINGS")
	class USkeletalMeshComponent* SkeletalMeshWeapon = nullptr;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"), Category = "CPP VARIABLES / WEAPON SETTINGS")
	class UStaticMeshComponent* StaticMeshWeapon = nullptr;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"), Category = "CPP VARIABLES / WEAPON SETTINGS")
	class UArrowComponent* ShootLocation = nullptr;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"), Category = "CPP VARIABLES / WEAPON SETTINGS")
	class UArrowComponent* SleeveLocation = nullptr;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"), Category = "CPP VARIABLES / WEAPON SETTINGS")
	class UArrowComponent* MagazineLocation = nullptr;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "CPP VARIABLES / WEAPON SETTINGS")
	FWeaponInfo WeaponSetting;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "CPP VARIABLES / WEAPON SETTINGS")
	FAdditionalWeaponInfo AddWeaponSetting;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:
	// Tick func
	virtual void Tick(float DeltaTime) override;

	void FireTick(float DeltaTime);
	void ReloadTick(float DeltaTime);
	void ShellTick(float DeltaTime);
	void MagazineTick(float DeltaTime);

	void WeaponInit(FWeaponInfo WeaponInfo);

	UFUNCTION()
	void InitDropMesh(UStaticMesh* Mesh, UArrowComponent* DropTransform, float LifeTime, float ImpulseStrength, float ImpulseDispersion, float MeshMass);

	bool DropShellFlag = false;
	float DropShellTimer = -1.0f;

	bool DropClipFlag = false;
	float DropClipTimer = -1.0f;


	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "FireLogic")
	bool WeaponFiring = false;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "FireLogic")
	bool IsReloading = false;

	UFUNCTION(BlueprintCallable)
	void SetWeaponStateFire(bool bIsFire);

	bool CheckWeaponCanFire();

	bool CanFire = true;

	FProjectileInfo GetProjectile();

	UFUNCTION()
	void Fire();

	void UpdateStateWeapon(EMovementState NewMovementState);
	void ChangeDispersion();

	//Timers'flags
	float FireTimer = 0.0f;
	float ReloadTimer = 0.0f;

	UFUNCTION(BlueprintCallable)
	int32 GetWeaponRound();


	int8 GetAvailableAmmoFromInventory();

	void WeaponReloadStart();

	void WeaponReloadEnd();

	void CancelReloading();

	bool CanWeaponReload();

	int8 GetNumberProjectileByShot();

	bool UsingSkeletalMesh = false;

	bool DoOnceReloadEndSound = false;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "AAAAAAAAAAAAAA")
	FVector WeaponLocation;

	FVector ShootEndLocation;

	float SizeVectorToChangeShootDirectionLogic = 200.0f;

	FVector GetFireEndLocation() const;

	FVector ApplyDispersionToShoot(FVector DirectionShoot) const;

	float GetCurrentDispersion() const;

	void DispersionTick(float DeltaTime);

	bool ShouldReduceDispersion = false;

	void ChangeDispersionByShot();

	float CurrentDispersion;
	float CurrentDispersionMax;
	float CurrentDispersionMin;
	float CurrentDispersionRecoil;
	float CurrentDispersionReduction;
};
