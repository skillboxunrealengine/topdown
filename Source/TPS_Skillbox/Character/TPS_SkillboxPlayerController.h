// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "Templates/SubclassOf.h"
#include "GameFramework/PlayerController.h"
#include "EnhancedInputComponent.h"
#include "EnhancedInputSubsystems.h"
#include "InputActionValue.h"
#include "TPS_SkillboxPlayerController.generated.h"

/** Forward declaration to improve compiling times */
class UNiagaraSystem;
class ATPS_SkillboxCharacter;

UCLASS()
class ATPS_SkillboxPlayerController : public APlayerController
{
	GENERATED_BODY()

public:
	ATPS_SkillboxPlayerController();

	UPROPERTY()
	ATPS_SkillboxCharacter* PlayerCharacter;

	bool IsCantRotate;

protected:

	// To add mapping context
	virtual void BeginPlay();

	virtual void OnPossess(APawn* InPawn) override;

	virtual void Tick(float DeltaSeconds) override;



	virtual void OnUnPossess() override;
};