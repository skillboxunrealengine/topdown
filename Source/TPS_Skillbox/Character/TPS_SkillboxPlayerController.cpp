// Copyright Epic Games, Inc. All Rights Reserved.

#include "TPS_SkillboxPlayerController.h"
#include "GameFramework/Pawn.h"
#include "Blueprint/AIBlueprintHelperLibrary.h"
#include "NiagaraSystem.h"
#include "NiagaraFunctionLibrary.h"
#include "Engine/World.h"
#include "Kismet/KismetMathLibrary.h"
#include "TPS_SkillboxCharacter.h"

ATPS_SkillboxPlayerController::ATPS_SkillboxPlayerController()
{
	bShowMouseCursor = true;
	DefaultMouseCursor = EMouseCursor::Default;
}

void ATPS_SkillboxPlayerController::BeginPlay()
{
	Super::BeginPlay();
}

void ATPS_SkillboxPlayerController::OnPossess(APawn* InPawn)
{
	Super::OnPossess(InPawn);

	if (InPawn) {
		PlayerCharacter = Cast<ATPS_SkillboxCharacter>(InPawn);
	}
}

void ATPS_SkillboxPlayerController::Tick(float DeltaSeconds)
{
	Super::Tick(DeltaSeconds);
}

void ATPS_SkillboxPlayerController::OnUnPossess()
{
	Super::OnUnPossess();
}
