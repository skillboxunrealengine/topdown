// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "TPS_Skillbox/PlayerStates.h"
#include "TPS_Skillbox/Weapon/WeaponDefaults.h"
#include "TPS_Skillbox/Interface/IGameActor.h"
#include "TPS_Skillbox/Effects/StateEffectBase.h"
#include "InputActionValue.h"
#include "EnhancedInputComponent.h"
#include "EnhancedInputSubsystems.h"

#include "TPS_SkillboxCharacter.generated.h"

class UGameInstanceBase;

UCLASS(Blueprintable)
class ATPS_SkillboxCharacter : public ACharacter, public IIGameActor
{
	GENERATED_BODY()

public:
	ATPS_SkillboxCharacter();

	FTimerHandle TimerHande_TimeToRagdoll;

	// ������ ����
	virtual void BeginPlay() override;

	// ����
	virtual void Tick(float DeltaSeconds) override;

	void LookTick(float DeltaTime);
	void StaminaTick(float DeltaTime);


	//  ����������  // -------------------------------------------------------------------------------
public:
	UPROPERTY()
	UEnhancedInputComponent* EnhancedInputComponent = nullptr;

protected:
	virtual void SetupPlayerInputComponent(UInputComponent* PlayerInputComponent) override;

	/** MappingContext */
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Input, meta = (AllowPrivateAccess = "true"))
	class UInputMappingContext* DefaultMappingContext;

	/** ������� ������������ */
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Input, meta = (AllowPrivateAccess = "true"))
	class UInputAction* SetMovementAction;

	/** ������� ���� */
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Input, meta = (AllowPrivateAccess = "true"))
	class UInputAction* SetRunAction;
	bool IsRunPressed;

	/** ������� ������������ */
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Input, meta = (AllowPrivateAccess = "true"))
	class UInputAction* SetAimAction;
	bool IsAimPressed;

	/** ������� ����� ������ �� �������� ���� */
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Input, meta = (AllowPrivateAccess = "true"))
	class UInputAction* SetSwitchWeaponAction;

	/** ������� ������� */
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Input, meta = (AllowPrivateAccess = "true"))
	class UInputAction* SetLookAction;

	/** ������� �������� */
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Input, meta = (AllowPrivateAccess = "true"))
	class UInputAction* SetAttackAction;

	/** ������� ����������� */
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Input, meta = (AllowPrivateAccess = "true"))
	class UInputAction* SetReloadAction;

	void OnMovementTriggered(const FInputActionValue& Value);

	void OnRunStarted();
	void OnRunCompleted();

	void OnAimStarted();
	void OnAimCompleted();

	void OnAttackStarted();
	void OnAttackCompleted();

	void DoAttack(bool IsFiring);
	void TryReloadStarted();

	void OnSwitchWeaponStarted(const FInputActionValue& Value);

public:
	FORCEINLINE class UCameraComponent* GetTopDownCameraComponent() const {
		return TopDownCameraComponent; 
	}

	FORCEINLINE class USpringArmComponent* GetCameraBoom() const {
		return CameraBoom; 
	}

	UPROPERTY()
	APlayerController* PlayerController = nullptr;


	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"), Category = "CPP VARIABLES / WEAPON SETTINGS")
	class UInventorySystemComponent* InventorySystemComponent = nullptr;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"), Category = "CPP VARIABLES / WEAPON SETTINGS")
	class UCharacterHealthSystemComponent* HealthSystemComponent = nullptr;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "CPP VARIABLES / Object Cursor")
	TSubclassOf<AActor> ObjectCursor;

	UFUNCTION()
	void ChangeMovementState(EMovementState State);

	UPROPERTY(BlueprintReadOnly, Replicated)
	EMovementState MovementState = EMovementState::Walk_state;

	UPROPERTY(BlueprintReadOnly)
	FCharacterSpeed MovementInfo;

	UFUNCTION()
	void RotatePlayer(bool Value);

	float RotateTo;
	float RotateToInterpolated;

	FRotator RotateToBefore;
	FRotator RotateToLerp;

	bool RotateFlag;

	UPROPERTY(BlueprintReadOnly)
	float MaxStamina = 125.0f;
	UPROPERTY(BlueprintReadOnly)
	float Stamina = 125.0f;
	float TimeToRegenerateStamina = 3.0f;
	float TimerRegenerateStamina = 0.0f;
	float StaminaRegenerate = 30.0f;


	bool rotated;

	float RotateLerpTimer;
	float RotateLerpTime = 0.25f;

	UGameInstanceBase* GI = nullptr;


	//  ������  // -------------------------------------------------------
	
	// ������� ������
	AWeaponDefaults* CurrentWeapon = nullptr;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "CPP VARIABLES / WEAPON SETTINGS")
	FAdditionalWeaponInfo AddWeaponSetting;

	UPROPERTY(BlueprintReadOnly)
	int32 CurrentIndexWeapon = 0;

	UFUNCTION(BlueprintCallable, BlueprintPure)
	AWeaponDefaults* GetCurrentWeapon();

	FVector Displacement = FVector(0.0f, 0.0f, 100.0f);

	UFUNCTION(BlueprintCallable)
	void InitWeapon(FName WeaponID, FAdditionalWeaponInfo WeaponAdditionalInfo, int32 Index);

	void TryReload();

	UFUNCTION()
	void WeaponFireStarted(UAnimMontage* AnimFire, UAnimMontage* AnimAimFire);
	UFUNCTION()
	void WeaponFireEnded();
	UFUNCTION()
	void WeaponReloadStarted(UAnimMontage* Anim);
	UFUNCTION()
	void WeaponReloadEnded(bool IsSuccess, int32 KeepAmmo);

	UFUNCTION(BlueprintNativeEvent)
	void WeaponFireStarted_BP(UAnimMontage* AnimFire, UAnimMontage* AnimAimFire);
	UFUNCTION(BlueprintNativeEvent)
	void WeaponFireEnded_BP();
	UFUNCTION(BlueprintNativeEvent)
	void WeaponReloadStarted_BP(UAnimMontage* Anim);
	UFUNCTION(BlueprintNativeEvent)
	void WeaponReloadEnded_BP(bool IsSuccess);

	UFUNCTION()
	void TrySwitchNextWeapon();

	UFUNCTION()
	void TrySwitchPreviousWeapon();



	virtual float TakeDamage(float DamageAmount, FDamageEvent const& DamageEvent, AController* EventInstigator, AActor* DamageCauser) override;

	UFUNCTION(BlueprintCallable)
	bool IsPlayerAlive();

	UFUNCTION(BlueprintCallable)
	void PlayerDeath();

	UFUNCTION(BlueprintNativeEvent)
	void PlayerDeath_BP();

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Death Animation", meta = (AllowPrivateAccess = "true"))
	TArray<UAnimMontage*> DeathMontages;
	
private:
	/** Top down camera */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	class UCameraComponent* TopDownCameraComponent;

	/** Camera boom positioning the camera above the character */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	class USpringArmComponent* CameraBoom;

	void EnableRagdoll();

	bool IsAlive = true;

	///
	/// �������
	///
	TArray<UStateEffectBase*> Effects;

	// INTERFACE -------------------------------------------------------------------------------------------------------------
	EPhysicalSurface GetSurfaceType() override;
	TArray<UStateEffectBase*> GetAllCurrentEffects() override;
	void RemoveEffect(UStateEffectBase* RemoveEffect) override;
	void AddEffect(UStateEffectBase* NewEffect) override;

	UFUNCTION(Server, Unreliable)
	void SetActorRotation_OnServer(float Yaw);
	UFUNCTION(NetMulticast, Unreliable)
	void SetActorRotation_Multicast(float Yaw);

	UFUNCTION(Server, Reliable)
	void SetMovementState_OnServer(EMovementState NewState);
	UFUNCTION(NetMulticast, Reliable)
	void SetMovementState_Multicast(EMovementState NewState);
};